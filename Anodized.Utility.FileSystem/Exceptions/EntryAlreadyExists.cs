﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anodized.Utility.FileSystem.Exceptions
{
    public class EntryAlreadyExists : Exception
    {
        public IFileSystemEntry Entry { get; }

        public EntryAlreadyExists(IFileSystemEntry entry) : base($"File system entry already exists: '{entry.FullPath}'")
        {
            Entry = entry;
        }
    }
}
