﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anodized.Utility.FileSystem.Exceptions
{
    public class EntryDoesNotExist : Exception
    {
        public IFileSystemEntry Entry { get; }

        public EntryDoesNotExist(IFileSystemEntry entry) : base($"Entry does not exist: '{entry.FullPath}'")
        {
            Entry = entry;
        }
    }
}
