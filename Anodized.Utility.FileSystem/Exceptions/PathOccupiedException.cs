﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Anodized.Utility.FileSystem.Exceptions
{
    public class PathOccupiedException : Exception
    {
        public string Path { get; }

        public PathOccupiedException(string path) : base($"Path '{path}' is already occupied by another entry")
        {
            Path = path;
        }
    }
}
