using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Anodized.Utility.FileSystem.Exceptions;
using Anodized.Utility.FileSystem.Extensions;
using Anodized.Utility.FileSystem.Helpers;
using Anodized.Utility.FileSystem.Watchers;

using DotNet.Globbing;

using JetBrains.Annotations;

using JsTimers;

namespace Anodized.Utility.FileSystem
{
    [PublicAPI]
    [DebuggerDisplay("Folder: {FullPath}")]
    public class Folder : IFileSystemEntry
    {
        static readonly Dictionary<string, WeakReference<Folder>> Cache =
            new Dictionary<string, WeakReference<Folder>>();

        readonly FolderWatcher   _watcher;
        readonly HashSet<string> _directChildFiles, _directChildFolders;

        bool           _exists, _readonly, _hidden;
        FileAttributes _attributes;
        DateTime       _creationTimeUtc, _lastAccessTimeUtc, _lastWriteTimeUtc;

        public string FullPath { get; }
        public string Name { get; }
        public bool Exists => _exists;
        public FileAttributes Attributes => _attributes;
        public DateTime CreationTime => _creationTimeUtc;
        public DateTime LastAccessTime => _lastAccessTimeUtc;
        public DateTime LastWriteTime => _lastWriteTimeUtc;
        public bool IsSymbolicLink => throw new NotImplementedException();
        public bool IsSystem => Attributes.HasFlag(FileAttributes.System);
        public bool IsTemp => Attributes.HasFlag(FileAttributes.Temporary);

        public bool IsReadonly
        {
            get => Attributes.HasFlag(FileAttributes.ReadOnly);
            set
            {
                if (!_exists)
                {
                    throw new EntryDoesNotExist(this);
                }

                if (value == _readonly)
                {
                    return;
                }

                if (value)
                {
                    System.IO.File.SetAttributes(FullPath, Attributes | FileAttributes.ReadOnly);
                }
                else
                {
                    System.IO.File.SetAttributes(FullPath, Attributes ^ FileAttributes.ReadOnly);
                }
            }
        }

        public bool IsHidden
        {
            get => Attributes.HasFlag(FileAttributes.Hidden);
            set
            {
                if (!_exists)
                {
                    throw new EntryDoesNotExist(this);
                }

                if (value == _hidden)
                {
                    return;
                }

                if (value)
                {
                    System.IO.File.SetAttributes(FullPath, Attributes | FileAttributes.Hidden);
                }
                else
                {
                    System.IO.File.SetAttributes(FullPath, Attributes ^ FileAttributes.Hidden);
                }
            }
        }

        public bool IsEncrypted => Attributes.HasFlag(FileAttributes.Encrypted);

        public bool IsPathOccupied => PathHelper.IsPathOccupied(FullPath);

        public bool IsRoot { get; }

        public FolderWatcher Watcher => _watcher;

        Folder(string path)
        {
            FullPath = path;
            Name = Path.GetFileName(FullPath);
            _directChildFiles = new HashSet<string>();
            _directChildFolders = new HashSet<string>();

            IsRoot = Path.GetPathRoot(FullPath).Equals(FullPath);

            _exists = PathHelper.IsFolder(FullPath);

            _watcher = RootWatcher.Mount(FullPath).WatchFolder(this);

            void DeleteHandler()
            {
                _attributes = 0;
                _readonly = _hidden = false;
                _creationTimeUtc = _lastAccessTimeUtc = _lastWriteTimeUtc = default;
                _directChildFiles.Clear();
                _directChildFolders.Clear();
            }

            void CreateHandler()
            {
                _attributes = System.IO.File.GetAttributes(FullPath);

                _readonly = _attributes.HasFlag(FileAttributes.ReadOnly);
                _hidden = _attributes.HasFlag(FileAttributes.ReadOnly);

                _creationTimeUtc = Directory.GetCreationTimeUtc(FullPath);
                _lastAccessTimeUtc = Directory.GetLastAccessTimeUtc(FullPath);
                _lastWriteTimeUtc = Directory.GetLastWriteTimeUtc(FullPath);

                string[] filePaths = Directory.GetFiles(FullPath);

                for (int i = 0; i < filePaths.Length; i++)
                {
                    _directChildFiles.Add(filePaths[i]);
                }

                string[] folderPaths = Directory.GetDirectories(FullPath);

                for (var i = 0; i < folderPaths.Length; i++)
                {
                    _directChildFolders.Add(folderPaths[i]);
                }
            }

            if (_exists)
            {
                CreateHandler();
            }

            _watcher.OnCreated += () =>
            {
                _exists = true;
                CreateHandler();
            };
            _watcher.OnRenamedFrom += _ =>
            {
                _exists = true;
                CreateHandler();
            };

            _watcher.OnDeleted += () =>
            {
                _exists = false;
                DeleteHandler();
            };
            _watcher.OnRenamedTo += _ =>
            {
                _exists = false;
                DeleteHandler();
            };

            _watcher.OnReadonlyChanged += bReadonly => TimerManager.SetImmediate(() => _readonly = bReadonly);
            _watcher.OnHiddenChanged += bHidden => TimerManager.SetImmediate(() => _hidden = bHidden);

            _watcher.OnAttributesChanged += attrs => TimerManager.SetImmediate(() => _attributes = attrs);

            _watcher.OnCreationTimeUpdated +=
                dtCreation => TimerManager.SetImmediate(() => _creationTimeUtc = dtCreation);
            _watcher.OnLastAccessTimeUpdated +=
                dtAccess => TimerManager.SetImmediate(() => _lastAccessTimeUtc = dtAccess);
            _watcher.OnLastWriteTimeUpdated += dtWrite => TimerManager.SetImmediate(() => _lastWriteTimeUtc = dtWrite);

            _watcher.OnChildCreated += (childPath, bDirect) =>
            {
                if (!bDirect)
                {
                    return;
                }

                TimerManager.SetImmediate(
                    () =>
                    {
                        if (PathHelper.IsFile(childPath))
                        {
                            _directChildFiles.Add(childPath);
                        }
                        else if (PathHelper.IsFolder(childPath))
                        {
                            _directChildFolders.Add(childPath);
                        }
                    }
                );
            };
            _watcher.OnChildDeleted += (childPath, bDirect) =>
            {
                if (!bDirect)
                {
                    return;
                }

                TimerManager.SetImmediate(
                    () =>
                    {
                        // doublecheck if info is still actual

                        if (_directChildFiles.Contains(childPath) && !PathHelper.IsFile(childPath))
                        {
                            _directChildFiles.Remove(childPath);
                        }

                        if (_directChildFolders.Contains(childPath) && !PathHelper.IsFolder(childPath))
                        {
                            _directChildFolders.Remove(childPath);
                        }
                    }
                );
            };

            _watcher.OnChildRenamed += (oldPath, newPath, bDirect) =>
            {
                if (!bDirect)
                {
                    return;
                }

                TimerManager.SetImmediate(
                    () =>
                    {
                        if (PathHelper.IsFile(newPath))
                        {
                            _directChildFiles.Remove(oldPath);
                            _directChildFiles.Add(newPath);
                        }
                        else if (PathHelper.IsFolder(newPath))
                        {
                            _directChildFolders.Remove(oldPath);
                            _directChildFolders.Add(newPath);
                        }
                    }
                );
            };
        }

        public static Folder FromPath(string path, bool createIfNotExist = true)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Given path is invalid", nameof(path));
            }

            string resolvedPath = PathHelper.ResolvePath(path);

            if (TryGetFromCache(resolvedPath, out Folder folder))
            {
                return folder;
            }

            folder = new Folder(resolvedPath);

            Cache[resolvedPath] = new WeakReference<Folder>(folder, false);

            if (!folder.Exists && createIfNotExist && !folder.IsPathOccupied)
            {
                folder.Create(false);
            }

            return folder;
        }

        public static Folder FromPath(string basePath, string path, bool createIfNotExist = true)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Given path is invalid", nameof(path));
            }

            return FromPath(PathHelper.ResolvePath(basePath, path), createIfNotExist);
        }

        static bool TryGetFromCache(string path, out Folder folder)
        {
            if (!Cache.TryGetValue(path, out WeakReference<Folder> reference) || !reference.TryGetTarget(out folder))
            {
                Cache.Remove(path);
                folder = null;
                return false;
            }

            return true;
        }

        public void Create(bool ignoreError = true)
        {
            if (Exists)
            {
                if (!ignoreError)
                {
                    throw new EntryAlreadyExists(this);
                }
            }
            else
            {
                if (IsPathOccupied)
                {
                    if (!ignoreError)
                    {
                        throw new PathOccupiedException(FullPath);
                    }
                }
                else
                {
                    Directory.CreateDirectory(FullPath);
                    _exists = true;
                }
            }
        }

        public void Remove(bool ignoreError = true)
        {
            if (!Exists)
            {
                if (!ignoreError)
                {
                    throw new EntryDoesNotExist(this);
                }
            }
            else
            {
                //Directory.Delete(FullPath, true);

                var files = GetFiles(null, false);

                foreach (File file in files)
                {
                    file.Remove(false);
                }

                var folders = GetFolders(null, false);

                foreach (Folder folder in folders)
                {
                    folder.Remove(false);
                }

                Directory.Delete(FullPath);
                _exists = true;
            }
        }

        public Folder GetParent()
        {
            if (IsRoot)
            {
                return null;
            }

            string parentPath = Path.GetDirectoryName(FullPath);

            return FromPath(parentPath, false);
        }

        public IReadOnlyCollection<string> GetDirectChildPaths()
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            List<string> list = new List<string>(_directChildFiles);
            list.AddRange(_directChildFolders);

            list.Sort();

            return list;
        }

        public IReadOnlyCollection<string> GetDirectChildFilePaths()
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            return _directChildFiles;
        }

        public IReadOnlyCollection<string> GetDirectChildFolderPaths()
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            return _directChildFolders;
        }

        public IReadOnlyCollection<string> GetChildPathsRecursive()
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            List<string> list = new List<string>(_directChildFiles);

            foreach (string childFolder in _directChildFolders)
            {
                list.Add(childFolder);

                Folder folder = FromPath(childFolder, false);

                list.AddRange(folder.GetChildPathsRecursive());
            }

            return list;
        }

        public IEnumerable<string> GetChildFilePathsRecursive()
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            List<string> list = new List<string>(_directChildFiles);

            foreach (string childFolder in _directChildFolders)
            {
                Folder folder = FromPath(childFolder, false);

                list.AddRange(folder.GetChildFilePathsRecursive());
            }

            return list;
        }

        public IEnumerable<string> GetChildFolderPathsRecursive()
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            List<string> list = new List<string>(_directChildFolders);

            foreach (string path in list)
            {
                Folder folder = FromPath(path, false);

                list.AddRange(folder.GetChildFolderPathsRecursive());
            }

            return list;
        }

        public IReadOnlyCollection<IFileSystemEntry> GetEntries(string searchPattern = null, bool recursive = false)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            List<IFileSystemEntry> list = new List<IFileSystemEntry>();

            if (!recursive)
            {
                var childFiles = GetDirectChildFilePaths();
                var childFolders = GetDirectChildFolderPaths();

                if (string.IsNullOrEmpty(searchPattern))
                {
                    list.AddRange(childFiles.Select(f => File.FromPath(f, false)));
                    list.AddRange(childFolders.Select(f => FromPath(f, false)));
                }
                else
                {
                    Glob g = Glob.Parse(searchPattern);

                    foreach (string file in childFiles)
                    {
                        string fn = Path.GetFileName(file);

                        if (g.IsMatch(fn))
                        {
                            list.Add(File.FromPath(file, false));
                        }
                    }

                    foreach (string folder in childFolders)
                    {
                        string fn = Path.GetFileName(folder);

                        if (g.IsMatch(fn))
                        {
                            list.Add(Folder.FromPath(folder, false));
                        }
                    }
                }
            }
            else
            {
                var childFiles = GetChildFilePathsRecursive();
                var childFolders = GetChildFolderPathsRecursive();

                if (string.IsNullOrEmpty(searchPattern))
                {
                    list.AddRange(childFiles.Select(f => File.FromPath(f, false)));
                    list.AddRange(childFolders.Select(f => Folder.FromPath(f, false)));
                }
                else
                {
                    Glob g = Glob.Parse(searchPattern);

                    foreach (string file in childFiles)
                    {
                        string relPath = GetRelativePathOf(file);
                        string fn = Path.GetFileName(file);

                        if (g.IsMatch(relPath) || g.IsMatch(fn))
                        {
                            list.Add(File.FromPath(file, false));
                        }
                    }

                    foreach (string folder in childFolders)
                    {
                        string relPath = GetRelativePathOf(folder);
                        string fn = Path.GetFileName(folder);

                        if (g.IsMatch(relPath) || g.IsMatch(fn))
                        {
                            list.Add(Folder.FromPath(folder, false));
                        }
                    }
                }
            }

            return list;
        }

        public IReadOnlyCollection<File> GetFiles(string searchPattern = null, bool recursive = false)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            List<File> list = new List<File>();

            if (!recursive)
            {
                var childFiles = GetDirectChildFilePaths();

                if (string.IsNullOrEmpty(searchPattern))
                {
                    list.AddRange(childFiles.Select(f => File.FromPath(f, false)));
                }
                else
                {
                    Glob g = Glob.Parse(searchPattern);

                    foreach (string file in childFiles)
                    {
                        string fn = Path.GetFileName(file);

                        if (g.IsMatch(fn))
                        {
                            list.Add(File.FromPath(file));
                        }
                    }
                }
            }
            else
            {
                var childFiles = GetChildFilePathsRecursive();

                if (string.IsNullOrEmpty(searchPattern))
                {
                    list.AddRange(childFiles.Select(f => File.FromPath(f, false)));
                }
                else
                {
                    Glob g = Glob.Parse(searchPattern);

                    foreach (string file in childFiles)
                    {
                        string relPath = GetRelativePathOf(file);
                        string fn = Path.GetFileName(file);

                        if (g.IsMatch(relPath) || g.IsMatch(fn))
                        {
                            list.Add(File.FromPath(file, false));
                        }
                    }
                }
            }

            return list;
        }

        public IReadOnlyCollection<Folder> GetFolders(string searchPattern = null, bool recursive = false)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            List<Folder> list = new List<Folder>();

            if (_directChildFolders.Count == 0)
            {
                return list;
            }

            if (!recursive)
            {
                var childFolders = GetDirectChildFolderPaths();

                if (string.IsNullOrEmpty(searchPattern))
                {
                    list.AddRange(childFolders.Select(f => FromPath(f, false)));
                }
                else
                {
                    Glob g = Glob.Parse(searchPattern);

                    foreach (string folder in childFolders)
                    {
                        string fn = Path.GetFileName(folder);

                        if (g.IsMatch(fn))
                        {
                            list.Add(FromPath(folder, false));
                        }
                    }
                }
            }
            else
            {
                var childFolders = GetChildFolderPathsRecursive();

                if (string.IsNullOrEmpty(searchPattern))
                {
                    list.AddRange(childFolders.Select(f => FromPath(f, false)));
                }
                else
                {
                    Glob g = Glob.Parse(searchPattern);

                    foreach (string folder in childFolders)
                    {
                        string relPath = GetRelativePathOf(folder);
                        string fn = Path.GetFileName(folder);

                        if (g.IsMatch(relPath) || g.IsMatch(fn))
                        {
                            list.Add(Folder.FromPath(folder, false));
                        }
                    }
                }
            }

            return list;
        }

        public IFileSystemEntry GetEntry(string searchPattern = null, bool recursive = false)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            var children = (recursive ? GetDirectChildPaths() : GetChildPathsRecursive()).OrderByDescending(p => p)
                .ToArray();

            if (children.Length == 0)
            {
                return null;
            }

            if (string.IsNullOrEmpty(searchPattern))
            {
                string first = children.First();

                if (_directChildFiles.Contains(first))
                {
                    return File.FromPath(first, false);
                }

                return FromPath(first, false);
            }

            Glob g = Glob.Parse(searchPattern);

            string child = children.FirstOrDefault(
                c =>
                {
                    string relPath = GetRelativePathOf(c);
                    string fn = Path.GetFileName(c);

                    return g.IsMatch(relPath) || g.IsMatch(fn);
                }
            );

            if (child != null)
            {
                if (PathHelper.IsFile(child))
                {
                    return File.FromPath(child, false);
                }

                return FromPath(child, false);
            }

            return null;
        }

        public File GetFile(string searchPattern = null, bool recursive = false)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            var childFiles =
                (recursive ? GetChildFilePathsRecursive() : GetDirectChildFilePaths()).OrderByDescending(p => p)
                .ToArray();

            if (childFiles.Length == 0)
            {
                return null;
            }

            if (string.IsNullOrEmpty(searchPattern))
            {
                return File.FromPath(childFiles.First(), false);
            }

            Glob g = Glob.Parse(searchPattern);

            string child = childFiles.FirstOrDefault(
                p =>
                {
                    string relPath = GetRelativePathOf(p);
                    string fn = Path.GetFileName(p);

                    return g.IsMatch(relPath) || g.IsMatch(fn);
                }
            );

            if (child != null)
            {
                return File.FromPath(child, false);
            }

            return null;
        }

        public Folder GetFolder(string searchPattern = null, bool recursive = false)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            if (_directChildFolders.Count == 0)
            {
                return null;
            }

            var childFolders =
                (recursive ? GetChildFolderPathsRecursive() : GetDirectChildFolderPaths()).OrderByDescending(p => p);

            if (string.IsNullOrEmpty(searchPattern))
            {
                return Folder.FromPath(childFolders.First(), false);
            }

            Glob g = Glob.Parse(searchPattern);

            string child = childFolders.FirstOrDefault(
                f =>
                {
                    string relPath = GetRelativePathOf(f);
                    string fn = Path.GetFileName(f);

                    return g.IsMatch(relPath) || g.IsMatch(fn);
                }
            );

            if (child != null)
            {
                return FromPath(child, false);
            }

            return null;
        }

        public IReadOnlyCollection<File> FlattenFiles()
        {
            return GetFiles(recursive: true);
        }

        public string GetRelativePathOf(IFileSystemEntry entry)
        {
            if (entry == null)
            {
                throw new ArgumentNullException(nameof(entry));
            }

            return GetRelativePathOf(entry.FullPath);
        }

        public string GetRelativePathOf(string path)
        {
#if NETSTANDARD2_0
            return PathHelper.GetRelativePathPolyfill(FullPath, path);
#else
            return Path.GetRelativePath(FullPath, path);
#endif
        }

        public string GetRelativePathTo(Folder folder)
        {
            if (folder == null)
            {
                throw new ArgumentNullException(nameof(folder));
            }

            return GetRelativePathTo(folder.FullPath);
        }

        public string GetRelativePathTo(string path)
        {
#if NETSTANDARD2_0
            return PathHelper.GetRelativePathPolyfill(path, FullPath);
#else
            return Path.GetRelativePath(path, FullPath);
#endif
        }

        public void AddCopy(IFileSystemEntry entry)
        {
            entry.CopyTo(this);
        }

        public File AddCopy(File file)
        {
            return file.CopyTo(this);
        }

        public Folder AddCopy(Folder folder)
        {
            return folder.CopyTo(this);
        }

        public void AddMove(IFileSystemEntry entry)
        {
            entry.MoveTo(this);
        }

        public File AddMove(File file)
        {
            return file.MoveTo(this);
        }

        public Folder AddMove(Folder folder)
        {
            return folder.MoveTo(this);
        }

        public Folder CopyTo(Folder folder, string newName = null) // subfolders with matching names should be merged
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            if (folder == null)
            {
                throw new ArgumentNullException(nameof(folder));
            }

            string dirName = newName ?? Name;

            string targetPath = Path.Combine(folder.FullPath, dirName);

            if (!folder.Exists)
            {
                folder.Create(false);
            }
            else if (PathHelper.IsFile(targetPath))
            {
                throw new PathOccupiedException(targetPath);
            }

            Folder targetFolder = FromPath(targetPath, true);

            var childFolders = GetChildFolderPathsRecursive();

            foreach (string f in childFolders)
            {
                string relPath = GetRelativePathOf(f);

                string tp = Path.Combine(targetFolder.FullPath, relPath);

                Folder subf = FromPath(tp);

                if (!subf.Exists)
                {
                    subf.Create(false);
                }
            }

            var childFiles = GetChildFilePathsRecursive();

            foreach (string f in childFiles)
            {
                string relPath = GetRelativePathOf(f);

                string tp = Path.Combine(targetFolder.FullPath, relPath);

                File subf = File.FromPath(tp);

                subf.IO.Clear();

                File origf = File.FromPath(f);

                using var fs = origf.GetReadonlyStream(false);
                using var fs2 = subf.GetWritableStream(false);

                fs.CopyTo(fs2);
                fs2.Flush(true);
            }

            return targetFolder;
        }

        public Folder CopyTo(string path)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            Folder targetFolder = Folder.FromPath(path);

            string name = Name;

            return CopyTo(targetFolder, name);
        }

        public Folder MoveTo(Folder folder, string newName = null)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            if (folder == null)
            {
                throw new ArgumentNullException(nameof(folder));
            }

            Folder newFolder = CopyTo(folder, newName);

            Remove(false);

            return newFolder;
        }

        public Folder MoveTo(string path)
        {
            Folder f = CopyTo(path);

            Remove(false);

            return f;
        }

#if NETSTANDARD2_0
        public bool HasAttribute(FileAttributes attributes)
        {
            return Attributes.HasFlag(attributes);
        }
#endif

        void IFileSystemEntry.CopyTo(Folder folder, string newName) => CopyTo(folder, newName);

        void IFileSystemEntry.CopyTo(string path) => CopyTo(path);

        void IFileSystemEntry.MoveTo(Folder folder, string newName) => MoveTo(folder, newName);

        void IFileSystemEntry.MoveTo(string path) => MoveTo(path);
    }
}
