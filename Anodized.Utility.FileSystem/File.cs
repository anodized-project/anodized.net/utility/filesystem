using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using Anodized.Utility.FileSystem.Exceptions;
using Anodized.Utility.FileSystem.Hash;
using Anodized.Utility.FileSystem.Helpers;
using Anodized.Utility.FileSystem.Watchers;

using JetBrains.Annotations;

using JsTimers;

using static Anodized.Utility.FileSystem.Helpers.PathHelper;

namespace Anodized.Utility.FileSystem
{
    [PublicAPI]
    [DebuggerDisplay("File: {FullPath}")]
    public class File : IFileSystemEntry
    {
        static readonly Dictionary<string, WeakReference<File>> Cache = new Dictionary<string, WeakReference<File>>();

        readonly FileWatcher _watcher;
        readonly FileIO      _io;
        readonly FileHash    _hash;

        bool           _readonly, _hidden, _exists;
        FileAttributes _attributes;
        DateTime       _creationTimeUtc, _lastAccessTimeUtc, _lastWriteTimeUtc;
        long           _size;

        public string FullPath { get; }
        public string Name { get; }
        public string Extension { get; }
        public string NameWithoutExtension { get; }
        public bool HasExtension { get; }
        public string FolderPath { get; }
        public bool Exists => _exists;
        public FileAttributes Attributes => _attributes;
        public DateTime CreationTime => _creationTimeUtc;
        public DateTime LastAccessTime => _lastAccessTimeUtc;
        public DateTime LastWriteTime => _lastWriteTimeUtc;
        public bool IsSymbolicLink => throw new NotImplementedException();
        public bool IsSystem => Attributes.HasFlag(FileAttributes.System);
        public bool IsTemp => Attributes.HasFlag(FileAttributes.Temporary);

        public bool IsReadonly
        {
            get => _readonly;
            set
            {
                if (!_exists)
                {
                    throw new EntryDoesNotExist(this);
                }

                if (value == _readonly)
                {
                    return;
                }

                if (value)
                {
                    System.IO.File.SetAttributes(FullPath, Attributes | FileAttributes.ReadOnly);
                }
                else
                {
                    System.IO.File.SetAttributes(FullPath, Attributes ^ FileAttributes.ReadOnly);
                }
            }
        }

        public bool IsHidden
        {
            get => _hidden;
            set
            {
                if (!_exists)
                {
                    throw new EntryDoesNotExist(this);
                }

                if (value == _hidden)
                {
                    return;
                }

                if (value)
                {
                    System.IO.File.SetAttributes(FullPath, Attributes | FileAttributes.Hidden);
                }
                else
                {
                    System.IO.File.SetAttributes(FullPath, Attributes ^ FileAttributes.Hidden);
                }
            }
        }

        public bool IsEncrypted => Attributes.HasFlag(FileAttributes.Encrypted);

        public bool IsPathOccupied => PathHelper.IsPathOccupied(FullPath);

        public long Size => _size;

        public FileIO IO => _io;
        public FileHash Hash => _hash;
        public FileWatcher Watcher => _watcher;

        File(string path)
        {
            FullPath = path;
            Name = Path.GetFileName(FullPath);
            Extension = Path.GetExtension(FullPath);
            HasExtension = Extension.Length != 0;
            #if NETSTANDARD2_0
            NameWithoutExtension = Name.Substring(0, Name.Length - Extension.Length);
            #else
            NameWithoutExtension = Name[..^Extension.Length];
            #endif
            FolderPath = Path.GetDirectoryName(FullPath);

            _exists = PathHelper.IsFile(FullPath);

            _io = new FileIO(this);
            _hash = new FileHash(this);

            _watcher = RootWatcher.Mount(FullPath).WatchFile(this);

            void DeleteHandler()
            {
                _attributes = 0;
                _creationTimeUtc = _lastAccessTimeUtc = _lastWriteTimeUtc = default;
                _size = 0;
                _readonly = false;
                _hidden = false;

                _hash.InvalidateCache();
            }

            void CreateHandler()
            {
                _creationTimeUtc = System.IO.File.GetCreationTimeUtc(FullPath);
                _lastAccessTimeUtc = System.IO.File.GetLastAccessTimeUtc(FullPath);
                _lastWriteTimeUtc = System.IO.File.GetLastWriteTimeUtc(FullPath);

                _attributes = System.IO.File.GetAttributes(FullPath);

                _readonly = _attributes.HasFlag(FileAttributes.ReadOnly);
                _hidden = _attributes.HasFlag(FileAttributes.Hidden);

                _size = new FileInfo(FullPath).Length;

                _hash.InvalidateCache();

                // forcefully recompute hashes AOT
                _hash.Md5Provider.Get();
                _hash.Sha1Provider.Get();
                _hash.Sha256Provider.Get();
            }

            if (_exists)
            {
                CreateHandler();
            }

            _watcher.OnCreated += () =>
            {
                _exists = true;
                CreateHandler();
            };
            _watcher.OnRenamedFrom += _ =>
            {
                _exists = true;
                CreateHandler();
            };

            _watcher.OnDeleted += () =>
            {
                _exists = false;
                DeleteHandler();
            };
            _watcher.OnRenamedTo += _ =>
            {
                _exists = false;
                DeleteHandler();
            };

            _watcher.OnReadonlyChanged += bReadonly => TimerManager.SetImmediate(() => _readonly = bReadonly);
            _watcher.OnHiddenChanged += bHidden => TimerManager.SetImmediate(() => _hidden = bHidden);
            _watcher.OnAttributesChanged += attrs => TimerManager.SetImmediate(() => _attributes = attrs);

            _watcher.OnCreationTimeUpdated +=
                dtCreation => TimerManager.SetImmediate(() => _creationTimeUtc = dtCreation);
            _watcher.OnLastAccessTimeUpdated +=
                dtAccess => TimerManager.SetImmediate(() => _lastAccessTimeUtc = dtAccess);
            _watcher.OnLastWriteTimeUpdated += dtWrite => TimerManager.SetImmediate(() => _lastWriteTimeUtc = dtWrite);

            _watcher.OnSizeChanged += (_, newSize) => TimerManager.SetImmediate(() => _size = newSize);
            //_watcher.OnContentChanged += (_, __, newHash, newSize) => TimerManager.SetImmediate(() => _hash.InvalidateCache());
        }

        public static File FromPath(string path, bool createIfNotExist = true)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Given path is invalid", nameof(path));
            }

            string resolvedPath = PathHelper.ResolvePath(path);

            if (TryGetFromCache(resolvedPath, out File file))
            {
                return file;
            }

            file = new File(resolvedPath);

            Cache[resolvedPath] = new WeakReference<File>(file, false);

            if (!file.Exists && createIfNotExist && !PathHelper.IsPathOccupied(resolvedPath))
            {
                file.Create(false);
            }

            return file;
        }

        public static File FromPath(string basePath, string path, bool createIfNotExist = true)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Given path is invalid", nameof(path));
            }

            return FromPath(PathHelper.ResolvePath(path, basePath), createIfNotExist);
        }

        static bool TryGetFromCache(string path, out File file)
        {
            if (!Cache.TryGetValue(path, out WeakReference<File> reference) || !reference.TryGetTarget(out file))
            {
                Cache.Remove(path);
                file = null;
                return false;
            }

            return true;
        }

        public Folder GetParent()
        {
            return Folder.FromPath(FolderPath, false);
        }

        public void Create(bool ignoreError = true)
        {
            if (Exists)
            {
                if (!ignoreError)
                {
                    throw new EntryAlreadyExists(this);
                }
            }
            else
            {
                if (IsPathOccupied)
                {
                    if (!ignoreError)
                    {
                        throw new PathOccupiedException(FullPath);
                    }
                }
                else
                {
                    GetParent().Create();
                    System.IO.File.Create(FullPath).Dispose();
                    _exists = true;
                }
            }
        }

        public void Remove(bool ignoreError = true)
        {
            if (!Exists)
            {
                if (!ignoreError)
                {
                    throw new EntryDoesNotExist(this);
                }
            }
            else
            {
                System.IO.File.Delete(FullPath);
                _exists = false;
            }
        }

        public string GetRelativePathTo(Folder folder)
        {
            if (folder == null)
            {
                throw new ArgumentNullException(nameof(folder));
            }

            return GetRelativePathTo(folder.FullPath);
        }

        public string GetRelativePathTo(string path)
        {
#if NETSTANDARD2_0
            return GetRelativePathPolyfill(path, FullPath);
#else
            return Path.GetRelativePath(path, FullPath);
#endif
        }

        public File CopyTo(Folder folder, string newName = null)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            if (folder == null)
            {
                throw new ArgumentNullException(nameof(folder));
            }

            string fileName = newName ?? Name;

            string targetPath = Path.Combine(folder.FullPath, fileName);

            if (!folder.Exists)
            {
                if (folder.IsPathOccupied)
                {
                    throw new PathOccupiedException(folder.FullPath);
                }

                folder.Create(false);
            }
            else if (PathHelper.IsFolder(targetPath))
            {
                throw new PathOccupiedException(targetPath);
            }

            File targetFile = FromPath(targetPath);
            targetFile.IO.Clear();

            using var fromStr = IO.GetReadonlyStream(false, false);
            using var toStr = targetFile.IO.GetWriteableStream(false, false);

            fromStr.CopyTo(toStr);
            toStr.Flush(true);

            return targetFile;
        }

        public File CopyTo(string path)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            // check if path is pointing to a dir
            string dirName;
            string fileName;

            if (IsPathPointingToFolder(path))
            {
                dirName = PathHelper.ResolvePath(path);
                fileName = Name;
            }
            else
            {
                dirName = Path.GetDirectoryName(PathHelper.ResolvePath(path));
                fileName = Path.GetFileName(path);
            }

            Folder folder = Folder.FromPath(dirName, false);

            return CopyTo(folder, fileName);
        }

        public File MoveTo(Folder folder, string newName = null)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            if (folder == null)
            {
                throw new ArgumentNullException(nameof(folder));
            }

            string fileName = newName ?? Name;

            string targetPath = Path.Combine(folder.FullPath, fileName);

            if (!folder.Exists)
            {
                if (folder.IsPathOccupied)
                {
                    throw new PathOccupiedException(folder.FullPath);
                }

                folder.Create(false);
            }
            else if (PathHelper.IsFolder(targetPath))
            {
                throw new PathOccupiedException(targetPath);
            }

            File targetFile = FromPath(targetPath);
            targetFile.IO.Clear();

            using var fromStr = IO.GetReadonlyStream(false, false);
            using var toStr = targetFile.IO.GetWriteableStream(false, false);

            fromStr.CopyTo(toStr);
            toStr.Flush(true);

            Remove(false);

            return targetFile;
        }

        public File MoveTo(string path)
        {
            if (!Exists)
            {
                throw new EntryDoesNotExist(this);
            }

            // check if path is pointing to a dir
            string dirName;
            string fileName;

            if (IsPathPointingToFolder(path))
            {
                dirName = PathHelper.ResolvePath(path);
                fileName = Name;
            }
            else
            {
                dirName = Path.GetDirectoryName(PathHelper.ResolvePath(path));
                fileName = Path.GetFileName(path);
            }

            Folder folder = Folder.FromPath(dirName, false);

            return MoveTo(folder, fileName);
        }

#if NETSTANDARD2_0
        public bool HasAttribute(FileAttributes attributes)
        {
            return Attributes.HasFlag(attributes);
        }
#endif

        void IFileSystemEntry.CopyTo(string path) => CopyTo(path);

        void IFileSystemEntry.CopyTo(Folder folder, string newName) => CopyTo(folder, newName);

        void IFileSystemEntry.MoveTo(string path) => MoveTo(path);

        void IFileSystemEntry.MoveTo(Folder folder, string newName) => MoveTo(folder, newName);
    }
}
