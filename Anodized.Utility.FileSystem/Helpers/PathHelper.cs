using System;
using System.IO;

namespace Anodized.Utility.FileSystem.Helpers
{
    public static class PathHelper
    {
        public static string ResolvePath(string path)
        {
            if (Path.IsPathRooted(path))
            {
                return Path.GetFullPath(path);
            }

            return ResolvePath(Environment.CurrentDirectory, path);
        }

        public static string ResolvePath(string basePath, string path)
        {
            if (string.IsNullOrWhiteSpace(basePath))
            {
                throw new ArgumentException("Base path is invalid", nameof(basePath));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Path is invalid", nameof(path));
            }

#if NETSTANDARD2_0
            return Path.GetFullPath(Path.Combine(basePath, path));
#else
            return Path.GetFullPath(path, basePath);
#endif
        }

        public static bool IsPathOccupied(string path)
        {
            return IsFile(path) || IsFolder(path);
        }

        public static bool IsPathOccupied(string basePath, string path)
        {
            return IsFile(basePath, path) || IsFolder(basePath, path);
        }

        public static bool IsFile(string path)
        {
            return System.IO.File.Exists(ResolvePath(path));
        }

        public static bool IsFile(string basePath, string path)
        {
            return System.IO.File.Exists(ResolvePath(basePath, path));
        }

        public static bool IsFolder(string path)
        {
            return Directory.Exists(ResolvePath(path));
        }

        public static bool IsFolder(string basePath, string path)
        {
            return Directory.Exists(ResolvePath(basePath, path));
        }

        public static bool IsPathPointingToFolder(string path)
        {
#if NETSTANDARD2_0
            return path.EndsWith(Path.DirectorySeparatorChar.ToString()) ||
                   path.EndsWith(Path.AltDirectorySeparatorChar.ToString()) ||
                   IsFolder(path);
#else
            return path.EndsWith(Path.DirectorySeparatorChar) ||
                   path.EndsWith(Path.AltDirectorySeparatorChar) ||
                   IsFolder(path);
#endif
        }

        public static string GetRelativePathPolyfill(string relativeTo, string fullPath)
        {
            string text = Path.GetDirectoryName(Path.GetFullPath(relativeTo));
            string text2 = "";
            while (text.Length > 0)
            {
                if (text.Length <= fullPath.Length &&
                    string.Compare(text, fullPath.Substring(0, text.Length), StringComparison.OrdinalIgnoreCase) == 0)
                {
                    text2 += fullPath.Substring(text.Length);
                    if (text2.StartsWith(Path.DirectorySeparatorChar.ToString() ?? "", StringComparison.Ordinal))
                    {
                        text2 = text2.Substring(1);
                    }

                    return text2;
                }

                text2 = text2 + ".." + Path.DirectorySeparatorChar.ToString();
                if (text.Length < 2)
                {
                    break;
                }

                int num = text.LastIndexOf(Path.DirectorySeparatorChar, text.Length - 2);
                text = text.Substring(0, num + 1);
            }

            return fullPath;
        }
    }
}
