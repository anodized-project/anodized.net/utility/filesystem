using System;
using System.Collections.Generic;
using System.Text;

namespace Anodized.Utility.FileSystem.Helpers
{
    public static class EnumHelper
    {
        public static List<T> FlagsAsList<T>(int flags) where T : Enum
        {
            Array array = Enum.GetValues(typeof(T));

            List<T> list = new List<T>();

            for (int i = 0; i < array.Length; i++)
            {
                object value = array.GetValue(i);
                int flag = (int)value;

                if ((flag & flags) != 0)
                {
                    list.Add((T)value);
                }
            }

            return list;
        }
    }
}
