using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

using Anodized.Utility.FileSystem.Exceptions;
using Anodized.Utility.FileSystem.Serialization;
using Anodized.Utility.FileSystem.Serialization.JsonNet;

using JetBrains.Annotations;

using Newtonsoft.Json;

namespace Anodized.Utility.FileSystem
{
    [PublicAPI]
    public class FileIO
    {
        public static Encoding            DefaultStringEncoding = Encoding.UTF8;
        public static ISerializer<string> DefaultStringSerializer;
        public static ISerializer<byte[]> DefaultBinarySerializer;

        static readonly string[] LineSplitSequences = { "\r\n", "\r", "\n" };

        File                     _file;
        Encoding                 _stringEncoding;
        ISerializer<string>      _stringSerializer;
        ISerializer<byte[]>      _binarySerializer;
        HashSet<Stream>          _openStreams;

        bool Exists => _file.Exists;

        static FileIO()
        {
            DefaultStringSerializer = new JsonNetSerializer(
                new JsonSerializerSettings {
                    Formatting = Formatting.Indented,
                    NullValueHandling = NullValueHandling.Include,
                    Culture = CultureInfo.InvariantCulture,
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                }
            );
        }

        public FileIO(File file)
        {
            _file = file;
            _stringEncoding = DefaultStringEncoding;
            _stringSerializer = DefaultStringSerializer;
            _binarySerializer = DefaultBinarySerializer;

            _openStreams = new HashSet<Stream>();
        }

        ~FileIO()
        {
            ReleaseOpenStreams();
        }

        #region General stream manipulation

        public FileStream GetReadonlyStream(bool createOnMissing = true, bool allowOtherWrite = true)
        {
            if (!Exists)
            {
                if (createOnMissing)
                {
                    if (_file.IsPathOccupied)
                    {
                        throw new PathOccupiedException(_file.FullPath);
                    }

                    _file.Create(false);
                }
                else
                {
                    throw new EntryDoesNotExist(_file);
                }
            }

            FileStream stream = new FileStream(
                _file.FullPath,
                FileMode.Open,
                FileAccess.Read,
                allowOtherWrite ? FileShare.ReadWrite : FileShare.Read
            );

            _openStreams.Add(stream);

            return stream;
        }

        public FileStream GetWriteableStream(bool createOnMissing = true, bool allowOtherWrite = true)
        {
            if (!Exists)
            {
                if (createOnMissing)
                {
                    if (_file.IsPathOccupied)
                    {
                        throw new PathOccupiedException(_file.FullPath);
                    }

                    _file.Create(false);
                }
                else
                {
                    throw new EntryDoesNotExist(_file);
                }
            }

            FileStream stream = new FileStream(
                _file.FullPath,
                FileMode.Open,
                FileAccess.ReadWrite,
                allowOtherWrite ? FileShare.ReadWrite : FileShare.Read
            );

            _openStreams.Add(stream);

            return stream;
        }

        public void Clear(bool createOnMissing = true)
        {
            if (!Exists)
            {
                if (createOnMissing)
                {
                    if (_file.IsPathOccupied)
                    {
                        throw new PathOccupiedException(_file.FullPath);
                    }

                    _file.Create(false);
                }

                return;
            }

            using FileStream stream = GetWriteableStream(false, false);

            stream.SetLength(0L);
            stream.Flush(true);
        }

        public void ReleaseOpenStreams()
        {
            Stream[] array = _openStreams.ToArray();

            for (var i = 0; i < array.Length; i++)
            {
                Stream stream = array[i];

                stream.Dispose();
            }

            _openStreams.Clear();
        }

        #endregion

        #region Read methods

        public string ReadText(bool throwOnMissing = false)
        {
            return ReadText(throwOnMissing, _stringEncoding);
        }

        public string ReadText(bool throwOnMissing, Encoding overrideEncoding)
        {
            if (overrideEncoding == null)
            {
                throw new ArgumentNullException(nameof(overrideEncoding));
            }

            if (!Exists)
            {
                if (throwOnMissing)
                {
                    throw new EntryDoesNotExist(_file);
                }

                return string.Empty;
            }

            using FileStream stream = GetReadonlyStream(false, false);

            using StreamReader reader = new StreamReader(stream, overrideEncoding);

            string text = reader.ReadToEnd();

            return text;
        }

        public string ReadLine(bool throwOnMissing = false, Encoding overrideEncoding = null, int offset = 0)
        {
            string[] lines = ReadLines(throwOnMissing, overrideEncoding, 1, offset);

            return lines[0];
        }

        public string[] ReadLines(
            bool throwOnMissing = false,
            Encoding overrideEncoding = null,
            int count = -1,
            int offset = 0
        )
        {
            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(offset), "Offset cannot be less than zero");
            }

            if (!Exists)
            {
                if (throwOnMissing)
                {
                    throw new EntryDoesNotExist(_file);
                }

                return Array.Empty<string>();
            }

            if (count == 0)
            {
                return Array.Empty<string>();
            }

            Encoding enc = overrideEncoding ?? _stringEncoding;

            string text = ReadText(false, enc);

            string[] lines = text.Split(LineSplitSequences, StringSplitOptions.None);

            int leng = count < 0 ? lines.Length : count;

            string[] result = new string[leng];

            for (int i = offset, r = 0; i < lines.Length && r < result.Length; i++, r++)
            {
                string line = lines[i];
                result[r] = line;
            }

            return result;
        }

        public byte[] ReadBinaryData(bool throwOnMissing = false)
        {
            if (!Exists)
            {
                if (throwOnMissing)
                {
                    throw new EntryDoesNotExist(_file);
                }

                return Array.Empty<byte>();
            }

            using FileStream stream = GetReadonlyStream(false, false);

            byte[] result = new byte[stream.Length];

            for (var i = 0L; i < result.LongLength; i++)
            {
                byte b = (byte)stream.ReadByte();

                result[i] = b;
            }

            return result;
        }

        public object ReadTextObj(bool throwOnMissing = false, Encoding overrideEncoding = null, Type type = null)
        {
            if (_stringSerializer == null)
            {
                throw new InvalidOperationException("Cannot deserialize data: String serializer is null");
            }

            if (!Exists)
            {
                if (throwOnMissing)
                {
                    throw new EntryDoesNotExist(_file);
                }

                return null;
            }

            Encoding enc = overrideEncoding ?? _stringEncoding;

            string text = ReadText(false, enc);

            object obj;

            if (_stringSerializer is JsonNetSerializer jsonSerializer && type != null)
            {
                obj = jsonSerializer.Deserialize(text, type);
            }
            else
            {
                obj = _stringSerializer.Deserialize(text);
            }

            return obj;
        }

        public T ReadTextObj<T>(bool throwOnMissing = false, Encoding overrideEncoding = null)
        {
            if (_stringSerializer == null)
            {
                throw new InvalidOperationException("Cannot deserialize data: String serializer is null");
            }

            if (!Exists)
            {
                if (throwOnMissing)
                {
                    throw new EntryDoesNotExist(_file);
                }

                return default;
            }

            Encoding enc = overrideEncoding ?? _stringEncoding;

            string text = ReadText(false, enc);

            T obj = _stringSerializer.Deserialize<T>(text);

            return obj;
        }

        #endregion

        #region Write methods

        public void WriteText(string text, Encoding overrideEncoding = null)
        {
            if (text == null)
            {
                throw new ArgumentNullException(nameof(text));
            }

            if (!Exists)
            {
                if (_file.IsPathOccupied)
                {
                    throw new PathOccupiedException(_file.FullPath);
                }

                _file.Create(false);
            }

            using FileStream stream = GetWriteableStream(false, false);

            Encoding enc = overrideEncoding ?? _stringEncoding;

            byte[] bytes = enc.GetBytes(text);

#if NETSTANDARD2_0
            stream.Write(bytes, 0, bytes.Length);
#else
            stream.Write(bytes);
#endif
            stream.Flush(true);
        }

        public void WriteTextObj(object obj, Encoding overrideEncoding = null)
        {
            if (_stringSerializer == null)
            {
                throw new InvalidOperationException("Cannot serialize data: String serializer is null");
            }

            if (!Exists)
            {
                _file.Create(false);
            }

            string serialized = _stringSerializer.Serialize(obj);

            using FileStream stream = GetWriteableStream(false, false);

            Encoding enc = overrideEncoding ?? _stringEncoding;

            byte[] bytes = enc.GetBytes(serialized);

#if NETSTANDARD2_0
            stream.Write(bytes, 0, bytes.Length);
#else
            stream.Write(bytes);
#endif
            stream.Flush(true);
        }

        public void WriteBinaryData(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException();
            }

            if (!Exists)
            {
                if (_file.IsPathOccupied)
                {
                    throw new PathOccupiedException(_file.FullPath);
                }

                _file.Create(false);
            }

            using FileStream stream = GetWriteableStream(false, false);

#if NETSTANDARD2_0
            stream.Write(data, 0, data.Length);
#else
            stream.Write(data);
#endif
            stream.Flush(true);
        }

        #endregion
    }
}
