using System;
using System.IO;
using System.Security.Cryptography;

using Anodized.Utility.FileSystem.Exceptions;

namespace Anodized.Utility.FileSystem.Hash
{
    public sealed class HashProvider : IDisposable
    {
        readonly File          _file;
        readonly HashAlgorithm _algorithm;

        byte[] _cache;
        bool   _cacheOutdated;
        bool   _disposed;

        public bool IsDisposed => _disposed;

        public HashProvider(File targetFile, HashAlgorithm algorithm)
        {
            _file = targetFile;
            _algorithm = algorithm;
            _cacheOutdated = true;
        }

        ~HashProvider()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }

            _algorithm.Dispose();
            _disposed = true;
        }

        public void InvalidateCache()
        {
            CheckDisposed();

            _cacheOutdated = true;
        }

        public byte[] Get()
        {
            CheckDisposed();

            if (_cacheOutdated)
            {
                _cache = Compute();
                _cacheOutdated = false;
            }

            byte[] result = new byte[_cache.Length];
            _cache.CopyTo(result, 0);

            return result;
        }

        byte[] Compute()
        {
            // CheckDisposed();

            if (_file.Exists)
            {
                using FileStream stream = _file.IO.GetReadonlyStream(false);
                return _algorithm.ComputeHash(stream);
            }

            throw new EntryDoesNotExist(_file);
        }

        void CheckDisposed()
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException("HashProvider: " + _algorithm.GetType().FullName);
            }
        }
    }
}
