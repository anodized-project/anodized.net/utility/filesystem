using System;
using System.Diagnostics;
using System.Security.Cryptography;

using JetBrains.Annotations;

namespace Anodized.Utility.FileSystem.Hash
{
    [PublicAPI]
    [DebuggerDisplay("{Md5}")]
    public class FileHash
    {
        File _file;
        HashProvider _md5, _sha1, _sha256;

        public byte[] Md5 => _md5.Get();
        public byte[] Sha1 => _sha1.Get();
        public byte[] Sha256 => _sha256.Get();

        public HashProvider Md5Provider => _md5;
        public HashProvider Sha1Provider => _sha1;
        public HashProvider Sha256Provider => _sha256;

        public File File => _file;

        public FileHash(File targetFile)
        {
            _file = targetFile;
            _md5 = new HashProvider(targetFile, MD5.Create());
            _sha1 = new HashProvider(targetFile, SHA1.Create());
            _sha256 = new HashProvider(targetFile, SHA256.Create());
        }

        ~FileHash()
        {
            _md5.Dispose();
            _sha1.Dispose();
            _sha256.Dispose();

            GC.SuppressFinalize(_md5);
            GC.SuppressFinalize(_sha1);
            GC.SuppressFinalize(_sha256);
        }

        public void InvalidateCache(bool md5 = true, bool sha1 = true, bool sha256 = true)
        {
            if (md5)
            {
                _md5.InvalidateCache();
            }

            if (sha1)
            {
                _sha1.InvalidateCache();
            }

            if (sha256)
            {
                _sha256.InvalidateCache();
            }
        }
    }
}
