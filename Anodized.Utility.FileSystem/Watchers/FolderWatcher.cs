using System;
using System.IO;

namespace Anodized.Utility.FileSystem.Watchers
{
    public class FolderWatcher : EntryWatcherBase
    {
        readonly Folder _folder;
        bool            _exists;

        public Folder Folder => _folder;
        public bool Recursive { get; set; }

        public event Action<string, bool> OnChildCreated, OnChildDeleted; // last bool arg indicates if it is a direct child
        public event Action<string, string, bool> OnChildRenamed;

        public FolderWatcher(RootWatcher rootWatcher, Folder folder) : base(rootWatcher, folder.FullPath)
        {
            _folder = folder;
            _exists = CheckExists(false);
        }

        internal override void OnCreatedInternal(string renamedFrom = null)
        {
            if (renamedFrom != null)
            {
                InvokeRenamedFrom(renamedFrom);
            }
            else
            {
                InvokeCreated();
            }

            _exists = true;
        }

        internal override void OnDeletedInternal(string renamedTo = null)
        {
            if (renamedTo != null)
            {
                InvokeRenamedTo(renamedTo);
            }
            else
            {
                InvokeDeleted();
            }

            _exists = false;
        }

        protected override bool CheckExists()
        {
            return CheckExists(true);
        }

        protected override FileAttributes GetCurrentAttributes()
        {
            return _folder.Attributes;
        }

        protected override FileAttributes GetActualAttributes()
        {
            return System.IO.File.GetAttributes(path);
        }

        protected override DateTime GetCurrentCreationTime()
        {
            return _folder.CreationTime;
        }

        protected override DateTime GetActualCreationTime()
        {
            return Directory.GetCreationTimeUtc(path);
        }

        protected override DateTime GetCurrentAccessTime()
        {
            return _folder.LastAccessTime;
        }

        protected override DateTime GetActualAccessTime()
        {
            return Directory.GetLastAccessTimeUtc(path);
        }

        protected override DateTime GetCurrentWriteTime()
        {
            return _folder.LastWriteTime;
        }

        protected override DateTime GetActualWriteTime()
        {
            return Directory.GetLastWriteTimeUtc(path);
        }

        // methods for updating children states

        internal void OnChildCreatedInternal(string childPath)
        {
            string childDir = Path.GetDirectoryName(childPath);

            bool isDirectChild = childDir == path;

            if (!isDirectChild && !Recursive)
            {
                return;
            }

            OnChildCreated?.Invoke(childPath, isDirectChild);
        }

        internal void OnChildDeletedInternal(string childPath)
        {
            string childDir = Path.GetDirectoryName(childPath);

            bool isDirectChild = childDir == path;

            if (!isDirectChild && !Recursive)
            {
                return;
            }

            OnChildDeleted?.Invoke(childPath, isDirectChild);
        }

        internal void OnChildRenamedInternal(string oldPath, string newPath)
        {
            string childDir = Path.GetDirectoryName(oldPath);

            bool isDirectChild = childDir == path;

            if (!isDirectChild&& !Recursive)
            {
                return;
            }

            OnChildRenamed?.Invoke(oldPath, newPath, isDirectChild);
        }

        bool CheckExists(bool fromCache)
        {
            if (fromCache)
            {
                return _exists;
            }

            return Directory.Exists(path);
        }
    }
}
