using System;
using System.IO;
using System.Linq;

namespace Anodized.Utility.FileSystem.Watchers
{
    public sealed class FileWatcher : EntryWatcherBase
    {
        const int MD5_HASH_SIZE_BYTES = 16;

        readonly File _file;
        bool          _writeTimeUpdatedLastTick;
        bool          _exists;

        public File File => _file;

        public event Action<byte[], long, byte[], long> OnContentChanged; // args: old md5, old size, new md5, new size
        public event Action<long, long> OnSizeChanged; // args: old size, new size

        public FileWatcher(RootWatcher rootWatcher, File file) : base(rootWatcher, file.FullPath)
        {
            _file = file;
            OnLastWriteTimeUpdated += _ => _writeTimeUpdatedLastTick = true;
            _exists = CheckExists(false);
        }

        internal override void OnCreatedInternal(string renamedFrom = null)
        {
            if (renamedFrom != null)
            {
                InvokeRenamedFrom(renamedFrom);
            }
            else
            {
                InvokeCreated();
            }

            _writeTimeUpdatedLastTick = true;
            _exists = true;
        }

        internal override void OnDeletedInternal(string renamedTo = null)
        {
            if (renamedTo != null)
            {
                InvokeRenamedTo(renamedTo);
            }
            else
            {
                InvokeDeleted();
            }

            _file.Hash.InvalidateCache();
            _writeTimeUpdatedLastTick = false;
            _exists = false;
        }

        protected override bool CheckExists()
        {
            return CheckExists(true);
        }

        protected override void PollTick()
        {
            base.PollTick();

            if (!CheckExists()) return;

            if (_writeTimeUpdatedLastTick)
            {
                byte[] oldHash = GetCurrentHash();
                byte[] newHash = GetActualHash();

                //byte[] oldHash = _file.Hash.Md5Provider.Get();
                //_file.Hash.Md5Provider.InvalidateCache();
                //byte[] newHash = _file.Hash.Md5Provider.Get();

                if (!oldHash.SequenceEqual(newHash))
                {
                    long oldSize = GetCurrentSize();
                    long newSize = GetActualSize();

                    OnContentChanged?.Invoke(oldHash, oldSize, newHash, newSize);

                    if (oldSize != newSize)
                    {
                        OnSizeChanged?.Invoke(oldSize, newSize);
                    }
                }

                _writeTimeUpdatedLastTick = false;
            }
        }

        protected override FileAttributes GetCurrentAttributes()
        {
            return _file.Attributes;
        }

        protected override FileAttributes GetActualAttributes()
        {
            return System.IO.File.GetAttributes(path);
        }

        protected override DateTime GetCurrentCreationTime()
        {
            return _file.CreationTime;
        }

        protected override DateTime GetActualCreationTime()
        {
            return System.IO.File.GetCreationTimeUtc(path);
        }

        protected override DateTime GetCurrentAccessTime()
        {
            return _file.LastAccessTime;
        }

        protected override DateTime GetActualAccessTime()
        {
            return System.IO.File.GetLastAccessTimeUtc(path);
        }

        protected override DateTime GetCurrentWriteTime()
        {
            return _file.LastWriteTime;
        }

        protected override DateTime GetActualWriteTime()
        {
            return System.IO.File.GetLastWriteTimeUtc(path);
        }

        long GetCurrentSize()
        {
            return _file.Size;
        }

        long GetActualSize()
        {
            return new FileInfo(path).Length;
        }

        byte[] GetCurrentHash()
        {
            return _file.Hash.Md5;
        }

        byte[] GetActualHash()
        {
            _file.Hash.InvalidateCache(sha1: false, sha256: false);
            return GetCurrentHash();
        }

        bool CheckExists(bool fromCache)
        {
            if (fromCache)
            {
                return _exists;
            }

            return System.IO.File.Exists(path);
        }
    }
}
