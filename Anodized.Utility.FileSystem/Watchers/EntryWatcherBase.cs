using System;
using System.IO;

using JsTimers;

namespace Anodized.Utility.FileSystem.Watchers
{
    public abstract class EntryWatcherBase
    {
        internal readonly  RootWatcher rootWatcher;
        internal readonly  string      path;
        protected readonly Timeout     pollTimer;

        public event Action<bool> OnReadonlyChanged, OnHiddenChanged; // arg: new state, fires after OnAttributesChanged
        public event Action OnCreated, OnDeleted; // fires if file was naturally deleted/created
        public event Action<string> OnRenamedFrom, OnRenamedTo; // fires if file was renamed
        public event Action<FileAttributes> OnAttributesChanged;
        public event Action<DateTime> OnCreationTimeUpdated, OnLastAccessTimeUpdated, OnLastWriteTimeUpdated;

        protected EntryWatcherBase(RootWatcher rootWatcher, string path)
        {
            this.rootWatcher = rootWatcher;
            this.path = path;
            pollTimer = TimerManager.SetInterval(PollTick, 500);
            pollTimer.UnRef();
        }

        ~EntryWatcherBase()
        {
            TimerManager.ClearInterval(pollTimer);
            RootWatcher.WatcherDispose(this);
        }

        internal abstract void OnCreatedInternal(string renamedFrom = null);

        internal abstract void OnDeletedInternal(string renamedTo = null);

        protected abstract bool CheckExists();

        protected abstract FileAttributes GetCurrentAttributes();

        protected abstract FileAttributes GetActualAttributes();

        protected abstract DateTime GetCurrentCreationTime();

        protected abstract DateTime GetActualCreationTime();

        protected abstract DateTime GetCurrentAccessTime();

        protected abstract DateTime GetActualAccessTime();

        protected abstract DateTime GetCurrentWriteTime();

        protected abstract DateTime GetActualWriteTime();

        protected virtual void PollTick()
        {
            if (!CheckExists()) return;

            FileAttributes oldAttributes = GetCurrentAttributes();
            FileAttributes newAttributes = GetActualAttributes();

            if (oldAttributes != newAttributes)
            {
                bool oldReadonly = oldAttributes.HasFlag(FileAttributes.ReadOnly);
                bool newReadonly = newAttributes.HasFlag(FileAttributes.ReadOnly);

                if (oldReadonly != newReadonly)
                {
                    InvokeReadonlyChanged(newReadonly);
                }

                bool oldHidden = oldAttributes.HasFlag(FileAttributes.Hidden);
                bool newHidden = newAttributes.HasFlag(FileAttributes.Hidden);

                if (oldHidden != newHidden)
                {
                    InvokeHiddenChanged(newHidden);
                }

                InvokeAttributesChanged(newAttributes);
            }

            DateTime oldCreationTime = GetCurrentCreationTime();
            DateTime newCreationTime = GetActualCreationTime();

            if (oldCreationTime != newCreationTime)
            {
                OnCreationTimeUpdated?.Invoke(newCreationTime);
            }

            DateTime oldAccessTime = GetCurrentAccessTime();
            DateTime newAccessTime = GetActualAccessTime();

            if (oldAccessTime != newAccessTime)
            {
                OnLastAccessTimeUpdated?.Invoke(newAccessTime);
            }

            DateTime oldWriteTime = GetCurrentWriteTime();
            DateTime newWriteTime = GetActualWriteTime();

            if (oldWriteTime != newWriteTime)
            {
                OnLastWriteTimeUpdated?.Invoke(newWriteTime);
            }
        }

        protected void InvokeReadonlyChanged(bool state)
        {
            OnReadonlyChanged?.Invoke(state);
        }

        protected void InvokeHiddenChanged(bool state)
        {
            OnHiddenChanged?.Invoke(state);
        }

        protected void InvokeCreated()
        {
            OnCreated?.Invoke();
        }

        protected void InvokeDeleted()
        {
            OnDeleted?.Invoke();
        }

        protected void InvokeRenamedFrom(string from)
        {
            OnRenamedFrom?.Invoke(from);
            InvokeCreated();
        }

        protected void InvokeRenamedTo(string to)
        {
            OnRenamedTo?.Invoke(to);
            InvokeDeleted();
        }

        protected void InvokeAttributesChanged(FileAttributes newAttributes)
        {
            OnAttributesChanged?.Invoke(newAttributes);
        }
    }
}
