using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using Anodized.Utility.FileSystem.Helpers;

using JetBrains.Annotations;

namespace Anodized.Utility.FileSystem.Watchers
{
    [PublicAPI]
    public sealed class RootWatcher
    {
        static readonly Dictionary<string, RootWatcher> ActiveWatchers = new Dictionary<string, RootWatcher>(); // TODO: Move this to be weakreferences
                                                                                                                // because currently it negotiates the
                                                                                                                // purpose of weakrefs in file/folder cache

        public readonly string                            Root;
        readonly        Dictionary<string, FileWatcher>   _fileWatchers;
        readonly        Dictionary<string, FolderWatcher> _folderWatchers;
        readonly        FileSystemWatcher                 _watcher;

        public IReadOnlyDictionary<string, FileWatcher> FileWatchers => _fileWatchers;
        public IReadOnlyDictionary<string, FolderWatcher> FolderWatchers => _folderWatchers;

        RootWatcher(string root)
        {
            Root = root;

            _fileWatchers = new Dictionary<string, FileWatcher>();
            _folderWatchers = new Dictionary<string, FolderWatcher>();

            _watcher = new FileSystemWatcher {
                Filter = "",
                IncludeSubdirectories = true,
                NotifyFilter = NotifyFilters.DirectoryName | NotifyFilters.FileName,
                Path = root
            };

            SetupEvents();

            _watcher.EnableRaisingEvents = true;
        }

        public static RootWatcher Mount(string path)
        {
            string root = Path.GetPathRoot(path);

            if (ActiveWatchers.ContainsKey(root))
            {
                return ActiveWatchers[root];
            }

            if (!Directory.Exists(root))
            {
                throw new InvalidOperationException("Cannot mount root watcher on drive that does not exist");
            }

            RootWatcher watcher = new RootWatcher(root);

            ActiveWatchers[root] = watcher;

            return watcher;
        }

        internal static void WatcherDispose(EntryWatcherBase watcher)
        {
            RootWatcher rWatcher = watcher.rootWatcher;

            if (watcher is FileWatcher)
            {
                rWatcher._fileWatchers.Remove(watcher.path);
            }
            else
            {
                rWatcher._folderWatchers.Remove(watcher.path);
            }
        }

        public FileWatcher WatchFile(File file)
        {
            string path = file.FullPath;

            if (_fileWatchers.ContainsKey(path))
            {
                FileWatcher watcher = _fileWatchers[path];

                if (watcher.File == file)
                {
                    return watcher;
                }
            }

            FileWatcher watcher2 = new FileWatcher(this, file);

            _fileWatchers[path] = watcher2;

            return watcher2;
        }

        public FolderWatcher WatchFolder(Folder folder)
        {
            string path = folder.FullPath;

            if (_folderWatchers.ContainsKey(path))
            {
                FolderWatcher watcher = _folderWatchers[path];

                if (watcher.Folder == folder)
                {
                    return watcher;
                }
            }

            FolderWatcher watcher2 = new FolderWatcher(this, folder);

            _folderWatchers[path] = watcher2;

            return watcher2;
        }

        public bool IsFileWatched(string path)
        {
            string fullPath = PathHelper.ResolvePath(path);

            return _fileWatchers.ContainsKey(fullPath);
        }

        public bool IsFolderWatched(string path)
        {
            string fullPath = PathHelper.ResolvePath(path);

            return _folderWatchers.ContainsKey(fullPath);
        }

        void SetupEvents()
        {
            _watcher.Error += (_, args) => throw new AggregateException("RootWatcher failed", args.GetException());

            _watcher.Created += CreatedEventHandler;
            _watcher.Deleted += DeletedEventHandler;
            _watcher.Renamed += RenamedEventHandler;
        }

        void CreatedEventHandler(object _, FileSystemEventArgs args)
        {
            string path = args.FullPath;

            if (System.IO.File.Exists(path) && _fileWatchers.ContainsKey(path))
            {
                _fileWatchers[path].OnCreatedInternal();
            }
            else if (Directory.Exists(path) && _folderWatchers.ContainsKey(path))
            {
                _folderWatchers[path].OnCreatedInternal();
            }

            string p = path;

            while (true)
            {
                string parentPath = Path.GetDirectoryName(p);

                if (parentPath == null)
                {
                    break;
                }

                if (IsFolderWatched(parentPath))
                {
                    FolderWatcher fWatcher = _folderWatchers[parentPath];

                    fWatcher.OnChildCreatedInternal(path);
                }

                p = parentPath;
            }
        }

        void DeletedEventHandler(object _, FileSystemEventArgs args)
        {
            string path = args.FullPath;

            if (_fileWatchers.ContainsKey(path))
            {
                _fileWatchers[path].OnDeletedInternal();
            }

            if (_folderWatchers.ContainsKey(path))
            {
                _folderWatchers[path].OnDeletedInternal();
            }

            string p = path;

            while (true)
            {
                string parentPath = Path.GetDirectoryName(p);

                if (parentPath == null)
                {
                    break;
                }

                if (IsFolderWatched(parentPath))
                {
                    FolderWatcher fWatcher = _folderWatchers[parentPath];

                    fWatcher.OnChildDeletedInternal(path);
                }

                p = parentPath;
            }
        }

        void RenamedEventHandler(object _, RenamedEventArgs args)
        {
            string oldPath = args.OldFullPath;
            string newPath = args.FullPath;

            if (_fileWatchers.TryGetValue(oldPath, out FileWatcher watcher))
            {
                watcher.OnDeletedInternal(newPath);
            }

            if (_fileWatchers.TryGetValue(newPath, out FileWatcher watcher2))
            {
                watcher2.OnCreatedInternal(oldPath);
            }

            if (_folderWatchers.TryGetValue(oldPath, out FolderWatcher watcher3))
            {
                watcher3.OnDeletedInternal(newPath);
            }

            if (_folderWatchers.TryGetValue(newPath, out FolderWatcher watcher4))
            {
                watcher4.OnCreatedInternal(oldPath);
            }

            string p = oldPath;

            while (true)
            {
                string oldParentPath = Path.GetDirectoryName(p);

                if (oldParentPath == null)
                {
                    break;
                }

                if (IsFolderWatched(oldParentPath))
                {
                    FolderWatcher fWatcher = _folderWatchers[oldParentPath];

                    fWatcher.OnChildRenamedInternal(oldPath, newPath);
                }

                p = oldParentPath;
            }

            p = newPath;

            while (true)
            {
                string newParentPath = Path.GetDirectoryName(p);

                if (newParentPath == null)
                {
                    break;
                }

                if (IsFolderWatched(newParentPath))
                {
                    FolderWatcher fWatcher = _folderWatchers[newParentPath];

                    fWatcher.OnChildRenamedInternal(oldPath, newPath);
                }

                p = newParentPath;
            }
        }
    }
}
