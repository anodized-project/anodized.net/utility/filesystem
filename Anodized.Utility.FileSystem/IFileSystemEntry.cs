using System;
using System.IO;

using JetBrains.Annotations;

namespace Anodized.Utility.FileSystem
{
    [PublicAPI]
    public interface IFileSystemEntry
    {
        string FullPath { get; }
        string Name { get; }
        bool Exists { get; }
        FileAttributes Attributes { get; }
        DateTime CreationTime { get; }
        DateTime LastAccessTime { get; }
        DateTime LastWriteTime { get; }

        bool IsSymbolicLink { get; }
        bool IsSystem { get; }
        bool IsTemp { get; }
        bool IsEncrypted { get; }
        bool IsReadonly { get; set; }
        bool IsHidden { get; set; }

        bool IsPathOccupied { get; }

#if NETSTANDARD2_0
        bool HasAttribute(FileAttributes attribute);
#else
        public bool HasAttribute(FileAttributes attribute) => Attributes.HasFlag(attribute);
#endif

        void Create(bool ignoreError);

        void Remove(bool ignoreError);

        Folder GetParent();

        string GetRelativePathTo(Folder folder);

        string GetRelativePathTo(string path);

        void CopyTo(Folder folder, string newName = null);

        void CopyTo(string path);

        void MoveTo(Folder folder, string newName = null);

        void MoveTo(string path);
    }
}
