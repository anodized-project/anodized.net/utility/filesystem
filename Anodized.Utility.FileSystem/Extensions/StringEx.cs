using System;
using System.Collections.Generic;
using System.Text;

namespace Anodized.Utility.FileSystem.Extensions
{
    public static class StringEx
    {
        public static File AsFilePath(this string self, bool createIfNotExist = false)
        {
            return File.FromPath(self, createIfNotExist);
        }

        public static Folder AsFolderPath(this string self, bool createIfNotExist = false)
        {
            return Folder.FromPath(self, createIfNotExist);
        }
    }
}
