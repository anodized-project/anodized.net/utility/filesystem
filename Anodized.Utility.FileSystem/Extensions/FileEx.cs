using System.IO;

namespace Anodized.Utility.FileSystem.Extensions
{
    public static class FileEx
    {
        public static FileStream GetReadonlyStream(this File file, bool allowOtherWrite = true)
        {
            return file.IO.GetReadonlyStream(allowOtherWrite: allowOtherWrite);
        }

        public static FileStream GetWritableStream(this File file, bool allowOtherWrite = true)
        {
            return file.IO.GetWriteableStream(allowOtherWrite: allowOtherWrite);
        }

        public static string GetTextContent(this File file)
        {
            return file.IO.ReadText(false);
        }
    }
}
