using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Anodized.Utility.FileSystem.Extensions
{
    public static class DirectoryInfoEx
    {
        public static Folder ToFolder(this DirectoryInfo info)
        {
            string path = info.FullName;

            return Folder.FromPath(path, false);
        }
    }
}
