using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using File = Anodized.Utility.FileSystem.File;

namespace Anodized.Utility.FileSystem.Extensions
{
    public static class FileInfoEx
    {
        public static File ToFile(this FileInfo info)
        {
            string path = info.FullName;

            return File.FromPath(path, false);
        }
    }
}
