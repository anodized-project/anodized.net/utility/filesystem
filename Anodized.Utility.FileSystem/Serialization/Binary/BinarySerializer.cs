using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using JetBrains.Annotations;

namespace Anodized.Utility.FileSystem.Serialization.Binary
{
    [PublicAPI]
    public abstract class BinarySerializer : ISerializer
    {
        protected BinaryFormatter formatter;

        BinarySerializer()
        {
            formatter = new BinaryFormatter();
        }

        object ISerializer.Serialize(object @object)
        {
            return Serialize(@object);
        }

        object ISerializer.Deserialize(object data)
        {
            if (data is Stream stream)
            {
                return Deserialize(stream);
            }

            if (data is byte[] bytes)
            {
                MemoryStream mstr = new MemoryStream(bytes);

                return Deserialize(mstr, true);
            }

            throw new NotSupportedException($"Type {data.GetType().Name} is not supported by this serializer");
        }

        public TObject Deserialize<TObject>(object data)
        {
            return (TObject)((ISerializer)this).Deserialize(data);
        }

        public MemoryStream Serialize(object @object)
        {
            MemoryStream stream = new MemoryStream();

            formatter.Serialize(stream, @object);

            return stream;
        }

        public object Deserialize(Stream stream, bool disposeStream = false)
        {
            object @object = formatter.Deserialize(stream);

            if (disposeStream)
            {
                stream.Dispose();
            }

            return @object;
        }
    }
}
