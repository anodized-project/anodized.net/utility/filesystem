#if !NETSTANDARD

using System;
using System.Text.Json;

namespace Anodized.Utility.FileSystem.Serialization.Json
{
    public class JsonSerializer : ISerializer<string>
    {
        public static JsonSerializerOptions InitOptions;

        JsonSerializerOptions _options;

        public JsonSerializerOptions Options => _options;

        public JsonSerializer(JsonSerializerOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException();
            }

            _options = options;
        }

        public JsonSerializer() : this(InitOptions ?? new JsonSerializerOptions()) { }

        object ISerializer.Serialize(object @object)
        {
            return Serialize(@object);
        }

        TObject ISerializer.Deserialize<TObject>(object data)
        {
            if (data is string strData)
            {
                return Deserialize<TObject>(strData);
            }

            throw new NotSupportedException($"Type {data.GetType().Name} is not supported by this serializer");
        }

        public object Deserialize(string data)
        {
            return System.Text.Json.JsonSerializer.Deserialize(data, typeof(object), _options);
        }

        public object Deserialize(string data, Type type)
        {
            return System.Text.Json.JsonSerializer.Deserialize(data, type, _options);
        }

        public TObject Deserialize<TObject>(string data)
        {
            return System.Text.Json.JsonSerializer.Deserialize<TObject>(data, _options);
        }

        public object Deserialize(object data)
        {
            if (data is string strData)
            {
                return Deserialize(strData);
            }

            throw new NotSupportedException($"Type {data.GetType().Name} is not supported by this serializer");
        }

        public string Serialize(object @object)
        {
            return System.Text.Json.JsonSerializer.Serialize(@object, _options);
        }

        public string Serialize<TObject>(TObject @object)
        {
            return System.Text.Json.JsonSerializer.Serialize<TObject>(@object, _options);
        }

        public string Serialize(object @object, Type type)
        {
            return System.Text.Json.JsonSerializer.Serialize(@object, type, _options);
        }
    }
}

#endif
