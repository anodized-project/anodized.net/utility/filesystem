
namespace Anodized.Utility.FileSystem.Serialization
{
    public interface ISerializer
    {
        object Serialize(object @object);

        object Deserialize(object data);

        TObject Deserialize<TObject>(object data);
    }
}
