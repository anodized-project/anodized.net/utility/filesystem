using System;

using JetBrains.Annotations;

using Newtonsoft.Json;

namespace Anodized.Utility.FileSystem.Serialization.JsonNet
{
    [PublicAPI]
    public class JsonNetSerializer : ISerializer<string>
    {
        public static JsonSerializerSettings InitSettings;

        JsonSerializerSettings _settings;

        public JsonSerializerSettings Settings => _settings;

        public JsonNetSerializer(JsonSerializerSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException();
            }

            _settings = settings;
        }

        public JsonNetSerializer() : this(InitSettings ?? new JsonSerializerSettings()) { }

        object ISerializer.Serialize(object @object)
        {
            return Serialize(@object);
        }

        public object Deserialize(string data)
        {
            return JsonConvert.DeserializeObject(data, _settings);
        }

        public object Deserialize(string data, Type type)
        {
            return JsonConvert.DeserializeObject(data, type, _settings);
        }

        public TObject Deserialize<TObject>(string data)
        {
            return JsonConvert.DeserializeObject<TObject>(data, _settings);
        }

        public object Deserialize(object data)
        {
            if (data is string strData)
            {
                return Deserialize(strData);
            }

            throw new NotSupportedException($"Type {data.GetType().Name} is not supported by this serializer");
        }

        public TObject Deserialize<TObject>(object data)
        {
            if (data is string strData)
            {
                return Deserialize<TObject>(strData);
            }

            throw new NotSupportedException();
        }

        public string Serialize(object @object)
        {
            return JsonConvert.SerializeObject(@object, _settings);
        }

        public string Serialize<TObject>(TObject @object)
        {
            return Serialize(@object, typeof(TObject));
        }

        public string Serialize(object @object, Type type)
        {
            return JsonConvert.SerializeObject(@object, type, _settings);
        }
    }
}
