
namespace Anodized.Utility.FileSystem.Serialization
{
    public interface ISerializer<TData> : ISerializer
    {
        new TData Serialize(object @object);

        object Deserialize(TData data);
    }
}
