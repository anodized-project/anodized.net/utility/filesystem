using System.Collections.Generic;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Anodized.Utility.FileSystem.Test
{
    public static class Globals
    {
        public const string MOCKS_DIR_PATH            = "Mocks/";
        public const string MOCKS_TEXT_FILE_PATH        = "MockFile.txt";
        public const string MOCK_JSON_FILE_PATH        = "MockJson.jsonc";
        public const string MOCK_XML_FILE_PATH         = "MockXml.xml";
        public const string MOCKS_WRITABLE_DIR_PATH = "MocksWritable/";

        public const string TEXT_FILE_CONTENT = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n";

        public static readonly Dictionary<string, object> JsonFileStructure = new Dictionary<string, object>
        {
            ["what?"] = "Anodized.Utility.FileSystem tests",
            ["why?"] = "Because",
            ["mbarr?"] = new string[] { "ofc" }
        };

        public static readonly Dictionary<string, object> XmlFileStructure = new Dictionary<string, object>
        {
            ["Content"] = new Dictionary<string, object>
            {
                ["What"] = "Anodized.Utility.FileSystem tests",
                ["Why"] = "Because",
                ["SomeProps"] = new Dictionary<string, object>
                {
                    ["Ofcourse"] = "WHY;NOT"
                }
            }
        };

        public static string GetMocksDirectoryPath(TestContext ctx)
        {
            return Path.Combine(ctx.TestDeploymentDir, MOCKS_DIR_PATH);
        }

        public static string GetMocksWritableDirectoryPath(TestContext ctx)
        {
            return Path.Combine(ctx.TestDeploymentDir, MOCKS_WRITABLE_DIR_PATH);
        }
    }
}
