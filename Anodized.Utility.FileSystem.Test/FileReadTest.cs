using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Anodized.Utility.FileSystem.Extensions;

using FluentAssertions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Anodized.Utility.FileSystem.Test
{
    [TestClass]
    public class FileReadTest
    {
        static string MocksDirPath;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            MocksDirPath = Globals.GetMocksDirectoryPath(context);
        }

        [TestMethod]
        public void ReadAndCheckTextFileContent()
        {
            Folder mocksFolder = Folder.FromPath(MocksDirPath,false);

            File textFile = mocksFolder.GetFiles().FirstOrDefault(f => f.Name.Equals("MockFile.txt"));

            Assert.IsNotNull(textFile);
            Assert.IsTrue(textFile.Exists);

            string text = textFile.GetTextContent();

            text.Should().Be(Globals.TEXT_FILE_CONTENT);
        }

        [TestMethod]
        public void ReadAndCheckJsonFileContent()
        {
            //Folder mocksFolder = Folder.FromPath(MocksDirPath,false);

            //File jsonFile = mocksFolder.GetFiles().FirstOrDefault(f => f.Name.Equals("MockJson.jsonc"));

            //Assert.IsNotNull(jsonFile);
            //jsonFile.Exists.Should().BeTrue();

            ////jsonFile.IO.StringSerializer = new JsonSerializer(
            ////    new System.Text.Json.JsonSerializerOptions {
            ////        ReadCommentHandling = System.Text.Json.JsonCommentHandling.Skip,

            ////    }
            ////);

            ////var obj = jsonFile.IO.ReadObjectFromString<Dictionary<string, object>>();

            //Assert.IsNotNull(obj);

            //obj.Should().HaveSameCount(Globals.JsonFileStructure);
            //obj.Keys.Should().BeEquivalentTo(Globals.JsonFileStructure.Keys);
        }
    }
}
