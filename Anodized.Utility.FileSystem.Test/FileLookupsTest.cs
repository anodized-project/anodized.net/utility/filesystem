using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;

using Anodized.Utility.FileSystem.Extensions;

using FluentAssertions;
using FluentAssertions.Collections;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Anodized.Utility.FileSystem.Test
{
    [TestClass]
    public class FileLookupsTest
    {
        static string MocksDirPath;
        static DirectoryInfo MocksDirInfo;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            MocksDirPath = Globals.GetMocksDirectoryPath(context);

            MocksDirInfo = new DirectoryInfo(MocksDirPath);
        }

        [TestMethod]
        public void GetTopLevelFiles()
        {
            Folder mocksFolder = GetMocksFolder();

            List<File> topFiles = (List<File>)mocksFolder.GetFiles();

            Assert.AreEqual(topFiles.Count, 3);

            new GenericCollectionAssertions<string>(topFiles.Select(f => f.Extension)).Contain(
                new [] { ".txt", ".jsonc", ".xml" }
            );
        }

        [TestMethod]
        public void GetAllFiles()
        {
            //List<File> allFiles = GetMocksFolder().FlattenFiles();
            //// 9
            //allFiles.Count.Should().Be(9, string.Join(", ", allFiles.Select(f => f.Name)));
        }

        [TestMethod]
        public void GetNestedFiles()
        {
            var dir2Files = GetMocksFolder().GetFolders().First(f => f.Name.Equals("Dir2")).GetFiles();

            dir2Files.Count().Should().Be(2);

            var txtF = dir2Files.FirstOrDefault(f => f.NameWithoutExtension.Equals("Dir2File"));

            Assert.IsNotNull(txtF);

            var jsonF = dir2Files.FirstOrDefault(f => f.NameWithoutExtension.Equals("Dir2FileJson"));

            Assert.IsNotNull(jsonF);
        }

        [TestMethod]
        public void GetFileMethodTest()
        {
            Folder mocksDir = GetMocksFolder();

            File txtMockFile = mocksDir.GetFile("MockFile.txt");

            Assert.IsNotNull(txtMockFile);
            txtMockFile.Name.Should().Be("MockFile.txt");
            txtMockFile.Exists.Should().Be(true);
        }

        [TestMethod]
        public void GetFileMethodTestRecursive()
        {
            Folder mocksDir = GetMocksFolder();

            File lolcode = mocksDir.GetFile("SubSubDir1File.lol", true);

            Assert.IsNotNull(lolcode);

            lolcode.Name.Should().Be("SubSubDir1File.lol");
            lolcode.Exists.Should().BeTrue();
        }

        Folder GetMocksFolder()
        {
            Folder mocksFolder = Folder.FromPath(MocksDirPath, false);

            return mocksFolder;
        }
    }
}
